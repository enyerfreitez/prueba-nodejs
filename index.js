console.clear();

//Importaciones
const express = require('express');  //Importamos la libreria express
const dotenv = require('dotenv');  //Importamos la libreria dotenv para las variables de entorno
const mongoose = require('mongoose');  //Importamos la libreria dotenv para las variables de entorno
const cookieParser = require('cookie-parser'); //Para poder leer las cookies
const {authSession,sessions} = require('./app/routes/auth_session');  //Importamos las rutas
const accountRouter = require('./app/routes/account');  //Importamos las rutas
const publicationRouter = require('./app/routes/publication');  //Importamos las rutas
const categoryRouter = require('./app/routes/category');  //Importamos las rutas

//Inicializaciones

dotenv.config();// inicializacion
const expressApp = express(); // La inicializamos

const PORT = process.env.PORT;

//MIDDLEWARE
expressApp.use(cookieParser());//MIDDLEWARE Permite parcear la infomracion que se reciba a Json
expressApp.use(express.json());//MIDDLEWARE Permite parcear la infomracion que se reciba a Json
expressApp.use(express.text());//MIDDLEWARE Permite parcear la infomracion que se reciba a texto
expressApp.use("/account", accountRouter);//Uso las rutas como un MIDDLEWARE para que las pueda reconocer y usar Indico el prefijo de las rutas a las cuales se aplicara
//expressApp.use('/api/dashboard', verifyToken, dashboadRoutes); // De esta manera se puede usar un middlaware para las rutas de este prefijo, Funciona para validar tokens
expressApp.use("/publication", publicationRouter);
expressApp.use("/auth", authSession);//Uso las rutas como un MIDDLEWARE para que las pueda reconocer y usar Indico el prefijo de las rutas a las cuales se aplicara
expressApp.use("/category", categoryRouter);//Uso las rutas como un MIDDLEWARE para que las pueda reconocer y usar Indico el prefijo de las rutas a las cuales se aplicara


expressApp.get('/raiz', (req, res) => { res.send(); }); //A esta ruta no se aplica el MIDDLEWARE de account

const bootstrap = async () => { // Utilizamos una funcion ya que la conexion a mongo es una promesa
  await mongoose.connect(process.env.MONGODB_URL).catch(error => handleError(error)); // se conecta pasando la url de conexion e indicando la promesa
  
  expressApp.listen(PORT, (req, res) => {  // indicamos que al momento de recibir una peticion por el puerto 3000 realice una accion
    console.log(`Server Encendido ${PORT}`);
  });
}

bootstrap();// se ejecuta la conexion y el encendido del server