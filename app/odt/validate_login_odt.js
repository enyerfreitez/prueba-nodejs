//Validaciones.
const { Type } = require('@sinclair/typebox'); //Libreria para crear el esquema para la Validacion
const Ajv = require("ajv"); // Libreria para las validaciones
const AjvFormats = require("ajv-formats"); //Libreria para validar el formato
const AjvErrors = require("ajv-errors"); //Libreria para los mensajes de errores

//Aqui se crea el esquema que se va a usar en las validaciones
const schema =  Type.Object({ //Se indica primero que lo que se va a validar es una JSON
    email: Type.String({  //Se indica el campo a validar en el obj
        format: 'email',  // tipo del campo a validar
        errorMessage: { // mensaje de error que se entregara 
            type: 'El correo debe ser de tipo string', // Mensaje si el tipo no es correcto 
            format: 'El correo debe de contener un correo valido' // Mensaje si el formato no es valido
        }
    }),
    password: Type.String({
        errorMessage: {
            type: 'La contraseña ha de ser de tipo string',
        }
    }),
}, 
{ 
    additionalProperties: false, // si indica que se puede recibir otros datos en el obj aparte de los que se van a validar
    errorMessage: {
        additionalProperties: 'Solo debe poseer el email y pw',
    }
});


const ajv = new Ajv({allErrors : true});  // se crea una instancia de la clase de validacion inicando que se usaran mensajes
AjvFormats(ajv, ["email"]).addKeyword('kind').addKeyword('modifier'); // se inicializa la libreria para formatos indicando el formato que necesitamos 
AjvErrors(ajv); // Se inicializa la libreria para los mensajes de error

const validate = ajv.compile(schema) // se le envia a la funcion de validacion el esquema que se usarar para validar

const validateLoginDTO = (req, res, next) => { 
    const valid = validate(req.body) // se valida los datos recibidos en el body
    if (!valid) res.status(400).send(validate.errors); // en caso de no ser valido se envia el error 

    next(); // se sigue a la funcion de la ruta solicitada 
};

module.exports = validateLoginDTO;