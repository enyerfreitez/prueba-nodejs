const mongoose = require("mongoose");

const categoriesSchema = mongoose.Schema(
    {
        name: {
            type: String,
            required: true
        },
        description:{
            type: String,
        }
    },
    {
        timestamps: true
    }
);

const categoriesModel = mongoose.model('Category',categoriesSchema);

module.exports = categoriesModel;