const mongoose = require('mongoose');

const publicationSchema = mongoose.Schema(
    {
        title: {
            type: String,
            required: true,
            min: 6
        },
        content: {
            type: String,
            required: true
        },
        author:{
            type: mongoose.Types.ObjectId
        },
        categories:{
            type: Array,
            default: []
        }
    },
    {
        timestamps : true
    }
);

const publicationModel = mongoose.model('Publication', publicationSchema);

module.exports = publicationModel;