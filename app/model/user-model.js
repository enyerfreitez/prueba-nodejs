const mongoose = require('mongoose'); // se indica que se necesita la libreria mongoosee

//Creamos el esquema que usara el modelo de los usuarios
const userSchema = mongoose.Schema({
    name:{
        type: String,
        required: true,
        min: 6,
        max: 255
    }, // se indica el nombre del campo y el tipo
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: String
},
{
    timestamps: true
});

const userModel = mongoose.model('User', userSchema); // creamos el modelo pasando el nombre y el esquema ya creado

module.exports = userModel; //Exporto el modelo