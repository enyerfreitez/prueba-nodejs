const userModel = require('../model/user-model');  //Importamos una bd en archivo de prueba

const authByEmailPwd = async (email, password) => {
    const user = await userModel.findOne({email: email}).exec(); 
    if (!user) return {};
    if (user.password != password) return {};
    return user;
}

module.exports = authByEmailPwd;
