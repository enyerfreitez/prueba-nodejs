const publicationRouter = require('express').Router();;
const mongoose = require('mongoose');
const publicationModel = require('../model/publication-model');
const {sessions} = require('./auth_session');  //Importamos las rutas
let idUser = '';

//MIDDLEWARE
publicationRouter.use((req, res, next) => {   
    const { cookies } = req; // obtenemos las cookies de la peticion
    const userSession = sessions.find( (session) => session.sessionId === cookies.sessionId ); //buscamos en el array de sessiones si existe la session recibida
    if(userSession)  idUser = mongoose.Types.ObjectId(userSession._id);
    next(); //Indica la ruta a donde ira
  });

publicationRouter.get('/', async (req, res) => {
    const publications = await publicationModel.find({}).exec(); // Todas las solicitures a BD son promesas
    
    if(!publications) return res.send('It does not have created publications');
    return res.send({publications,sessions});
});

publicationRouter.post('/', async (req, res) => {
    const publicationData = req.body;
    try {
        if(!publicationData.title || !publicationData.content) throw "incomplete_params"; 
        const idAutor = {'author': idUser};

        const publicationDataAutor = Object.assign( publicationData, idAutor ); // agrego el usuario
        
        const newPublication = new publicationModel(publicationDataAutor);
    
        newPublication.save(function (error) { // guardamos la instancia e indicamos si existe un error
            if (error) throw error;
        });
    
        return res.send('Publication created');
    
    } catch (error) {
        return res.status(400).send(error);
    }
    
});

publicationRouter.get('/:id', async (req, res) => {
    const id = req.params.id;
    try {
        
        if(!id) throw 'require_id';

        const publication = await publicationModel.findById(id);

        if(!publication) throw 'Publication not exist' ;

        return res.send(publication);
    } catch (error) {
        return res.status(400).send(error);
    }
});

publicationRouter.get('/byUser/:id', async (req, res) => {
    const idUser =  req.params.id;
    try {
        if(!idUser) throw 'required_id';
        
        //Crear una reliacion al momendo de la busqueda

        /*
            Model.aggregate([ 
                {
                    $lookup: { //palabra reservada
                        from: 'users', //Modelo al cual se buscara la relacion
                        localField: 'author', // Campo local el cual se usara para la relacion
                        foreignField: '_id', // Campo del modelo a buscar
                        as: 'publicationUser', //nombre se se le dara a la realacion
                    }
                },
                {
                    $match: { // where's
                        author: mongoose.Types.ObjectId(idUser)
                    }
                }
            ]);
        */

        const publicationsbyUser = await publicationModel.aggregate([ 
            {
                $lookup: {
                    from: 'users',
                    localField: 'author',
                    foreignField: '_id',
                    as: 'user',
                }
            },
            {
                $match: {
                    author: mongoose.Types.ObjectId(idUser)
                }
            }
        ]);
        
        if(Object.keys(publicationsbyUser).length === 0)throw 'not_publications_by_user';

        res.send(publicationsbyUser);
    } catch ( error ) {
        if(error) res.status(400).send(error);
    }
})

publicationRouter.put('/:id', async (req, res) => {
    const id = req.params.id;
    try {
        if(!id) throw "require_id"; 

        if(Object.keys(req.body).length === 0) throw "incomplete_params"; 

        const publication = await publicationModel.findByIdAndUpdate({_id : id}, req.body);
    
        if(!publication)  throw 'Publication not exist';
    
        return res.send('publication update');
    
    } catch (error) {
        return res.status(400).send(error);
    }
});

publicationRouter.delete('/:id', async (req, res) =>{
    const id = req.params.id;
    try {
        if(!id) throw 'require_id';

        const publication = await publicationModel.findByIdAndDelete(id).exec();

        if(!publication)  throw 'Publication not exist';

        return res.send('Publication deleted');
    } catch (error) {
        return res.status(400).send(error);   
    }
});

module.exports = publicationRouter;