const {Router} = require('express');  //Importamos la libreria express
const { nanoid } = require('nanoid');
const userModel = require('../model/user-model');  //Importamos una bd en archivo de prueba
const authByEmailPwd = require('../helpers/auth-by-email-password');  //Importamos la funcion de logueo
const validateLoginDTO = require('../odt/validate_login_odt');  //Importamos las validaciones

const authSession = Router(); //Inicializo las rutas
const sessions = []; // array para guardar las sessiones 

//Logear
authSession.post("/session", validateLoginDTO, async (req, res) => { // de esta forma se puede usar un MIDDLEWARE para validar

  const { email, password } = req.body; // Accedo directamente a un dato del obj
  if (!email || !password) { return res.status(400).send({ errorMessage: 'Datos incompletos' }); }
  try {
      const { name, _id } = await authByEmailPwd(email, password);

      if (!name) throw new Error();
      const sessionId = nanoid(); //Para crear una sessionId para el logueo
      sessions.push({sessionId, _id }); //La guardamos en un array para manejar todas las sessiones
      res.cookie("sessionId", sessionId, { 
        httpOnly: true,
      }); //Devolvemos la sessionId como en la cookies de la peticion
      return res.send(`Usuario ${name} logueado`);

  } catch (e) {
      return res.status(401).send({ errorMessage: 'Datos invalidos' });
  }

});

authSession.get("/session", (req, res) => {
  const { cookies } = req; // obtenemos las cookies de la peticion
  if (!cookies.sessionId) return res.status(401).send({ errorMessage: 'Sin Cookies' }); //de no existir 

  const userSession = sessions.find( (session) => session.sessionId === cookies.sessionId ); //buscamos en el array de sessiones si existe la session recibida

  if (!userSession) return res.status(401).send({ errorMessage: 'Usuario no validado' }); // si no se encuentra el usuario no es valido

  const { name } = USER_BD.find( (user) => user._id === userSession._id ) // Se recupera el usuario segun si guid

  return res.send(`cookies obtenida para el usuario ${name}`)
});

module.exports = {authSession,sessions}; //Exporto todas las rutas creadas
