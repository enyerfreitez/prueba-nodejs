const express = require('express');  //Importamos la libreria express
const userModel = require('../model/user-model');  //Importamos El modelo usuario

const accountRouter = express.Router(); //Inicializo las rutas

//MIDDLEWARE
accountRouter.use((req, res, next) => {
  console.log('Paso por el MIDDLEWARE');
  next(); //Indica la ruta a donde ira
});

//NO NECESITO PONER EL PREFIJO DE LA RUTA PORQUE AL LLAMARLO EN EL INDEX YA FUE INDICADO

// Listas de usaurios
accountRouter.get("", async (req, res) => {
  const user = await userModel.find({});
  res.send(user);
});

// Buscar usuario
accountRouter.get("/:id", async (req, res) => {
    const user = await userModel.findById(req.params.id).exec(); 

    if (!user) { return res.status(404).send({ errorMessage: 'Usuario no existe' }); }

    return res.send(`Usuario encontrado su nombre es ${user.name}`);
});

// Crear una cuenta
accountRouter.post("/", async (req, res) => { // usamos el async ya que se usan promesas
    const userData = req.body; // Accedo directamente a un dato del obj
    try {

        if (!userData)  throw  'Datos incompletos'; 
        // Realizamos una busqueda en el modelo si el user existe. Se usa el .exec() para lo reconosca como una promesa
        const user = await userModel.findOne({email: userData.email}).exec(); 

        if (user) throw  `Usuario ya existe ${user.name}`;  

        const newUser = new userModel(userData);// Creamos una nueva instancia del modelo enviando los datos a guardar. 

        newUser.save(function (err) { // guardamos la instancia e indicamos si existe un error
            if (err) throw err;
        });

        return res.send(`Usuario ${userData.name} Creado`); 
    }   catch (error) {
        return res.status(400).send(error);
    }
});

// Actualizar una nombre de usuario
accountRouter.patch("/:id", async (req, res) => { //uso 
    const id = req.params.id;
    const { name } = req.body; // Accedo directamente a un dato del obj
    try {
        if(!id) throw "require_id"; 

        // permite buscar el usuario y actualizarlo. el primer parametro es el de busqueda seguido del dato a actuaizar con su valor
        // como el dato que actualizare y el campo comparten el mismo nombre pasar solo name es lo mismo que decir { name : name }
        const user = await userModel.findOneAndUpdate({id}, { name }); 

        if (!user) throw  'Usuario no existe'; 
        /*
            //Esta seria otra manera donde se busca el usuario 
            const user = await userModel.findById(req.params.guid).exec(); 

            //Se actualiza el dato 
            user.name = name;

            //Y luego se guarda
            await user.save());
        */
        return res.send(`Usuario actualizado`);

    }   catch (error) {
        return res.status(400).send(error);
    }
});

//Eliminar una cuenta
accountRouter.delete("/:id", async (req, res) => {
    const id = req.params.id;
    // permite buscar el usuario y eliminar. el  parametro es el de busqueda
    try {
        if(!id) throw "require_id"; 
        const user = await userModel.findOneAndDelete( {id} ); 

        if (!user) throw "Usuario no existe"; 

        return res.send(`Usuario ${user.name} Eliminado`);
    }   catch (error) {
        return res.status(400).send(error);
    }

});

module.exports = accountRouter; //Exporto todas las rutas creadas
