const categoriesRouters  = require('express').Router();
const categoryModel = require('../model/category-model');

categoriesRouters.get('/', async ( req, res ) => {
    try {
        const categoriesAll = await categoryModel.find({}).exec();
        if(Object.keys(categoriesAll).length === 0)   throw 'not_categories';
        return res.send(categoriesAll);
    } catch (error) {
        if(error) return res.status(404).send(error);
    }
});

categoriesRouters.get('/:id', async ( req, res) => {
    const id = req.params.id;
    try {
        if(!id) throw 'requered_id';
        const category =  await categoryModel.findById(id).exec();  
        if(!category) throw 'category_not_found';
        return res.send(category);
    }
    catch (error) {
        if(error) return res.status(404).send(error);
    }
});

categoriesRouters.post('/', async ( req, res ) => {
    const body = req.body;
    try {
        if(!body.name) throw 'requered_name';

        //Validaciones
        const categorySeach = await categoryModel.findOne({name : body.name}).exec();

        if(categorySeach) throw 'category_exist'
        
        const categoryNew = new categoryModel(body);
        categoryNew.save(function (err) { // guardamos la instancia e indicamos si existe un error
            if (err) throw err;
        });

        return res.send('Categoria registrada');
    }
    catch (error) {
        if(error) return res.status(400).send(error);
    }
});

categoriesRouters.put('/:id', async ( req, res ) => {
    const id = req.params.id;
    const body = req.body;
    try {
        if(!id || !body.name ) throw 'inconplete_params';
        
        const category = await categoryModel.findByIdAndUpdate(id , body).exec();
        
        if(!category) throw 'category_not_found';
        
        return res.send(`Categoria ${body.name} Actualizada`);
    }
    catch (error) {
        if(error) return res.status(400).send(error);
    } 
});

categoriesRouters.delete('/:id', async ( req, res ) => {
    const id = req.params.id;
    try {
        if(!id) throw 'requered_id';

        const category = await categoryModel.findByIdAndDelete(id).exec();   

        if(!category) throw 'category_not_found';
        
        return res.send(`Categoria ${category.name} Eliminada`);
    }
    catch (error) {
        if(error) return res.status(400).send(error);
    }
});

module.exports = categoriesRouters;
