const USER_BD =
[
  {
    "_id": "6295906d6b37f73ec4567985",
    "index": 0,
    "guid": "4ceed879-a2c1-4146-a2da-fd92358bf5e5",
    "name": "Enyer Freitez",
    "email": "enyer@prueba.com",
    "password" : "1234",
    "isActive": true,
    "balance": "$2,632.12",
    "picture": "http://placehold.it/32x32",
    "age": 26,
    "eyeColor": "blue",
    "gender": "male",
    "company": "DUFLEX",
    "phone": "+1 (887) 487-3501",
  },
  {
    "_id": "6295906d86da65ac6540d29b",
    "index": 1,
    "guid": "b24a9497-45d6-44da-a8d7-9feb2d1221d9",
    "name": "Maria Peraza",
    "email": "maria@prueba.com",
    "password" : "1234",
    "isActive": true,
    "balance": "$2,645.11",
    "picture": "http://placehold.it/32x32",
    "age": 21,
    "eyeColor": "brown",
    "gender": "male",
    "company": "ROCKABYE",
    "phone": "+1 (840) 578-2505",
  },
  {
    "_id": "6295906dbb20b42e075dc3bb",
    "index": 2,
    "guid": "1c3006b6-81f1-4dcd-8fb0-6e33ed69b91a",
    "isActive": true,
    "balance": "$3,450.87",
    "picture": "http://placehold.it/32x32",
    "age": 24,
    "eyeColor": "green",
    "name": "Rene Mcmillan",
    "gender": "female",
    "company": "QUILCH",
    "email": "renemcmillan@quilch.com",
    "phone": "+1 (966) 447-3891",
    "address": "617 Quentin Road, Guilford, Missouri, 4815",
    "about": "Sunt dolore aliqua voluptate aliquip aute voluptate nulla deserunt laborum duis in in. Quis eu ipsum deserunt ut esse sunt eiusmod cillum enim. Qui excepteur aute nulla culpa cillum pariatur consequat amet sunt proident id. Cupidatat proident mollit ex consectetur exercitation enim est incididunt commodo aliqua cillum ex sit. Do occaecat ea eiusmod minim officia exercitation ut velit exercitation tempor est duis minim. Labore fugiat anim pariatur tempor dolore magna exercitation nostrud ea consectetur dolor consequat occaecat id.\r\n",
    "registered": "2019-06-22T09:49:04 +04:00",
    "latitude": -63.463041,
    "longitude": 112.738924,
    "tags": [
      "ullamco",
      "ex",
      "cillum",
      "magna",
      "fugiat",
      "nulla",
      "dolor"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Cook Alexander"
      },
      {
        "id": 1,
        "name": "Mcgowan Hill"
      },
      {
        "id": 2,
        "name": "Noble Davis"
      }
    ],
    "greeting": "Hello, Rene Mcmillan! You have 10 unread messages.",
    "favoriteFruit": "strawberry"
  },
  {
    "_id": "6295906dcdbd7339efbd5c0c",
    "index": 3,
    "guid": "cc15bfda-6941-4e3f-b57f-2a360c50dc09",
    "isActive": false,
    "balance": "$1,855.35",
    "picture": "http://placehold.it/32x32",
    "age": 23,
    "eyeColor": "brown",
    "name": "Rhonda Booker",
    "gender": "female",
    "company": "COSMOSIS",
    "email": "rhondabooker@cosmosis.com",
    "phone": "+1 (895) 507-2315",
    "address": "380 Aviation Road, Jeff, Virginia, 6132",
    "about": "Veniam irure nostrud enim sit. Officia exercitation consectetur ipsum fugiat ipsum ullamco irure. Esse excepteur ex fugiat amet ea commodo dolore. Excepteur incididunt officia quis ipsum adipisicing nulla enim mollit amet adipisicing officia reprehenderit consectetur. Laboris quis tempor ea reprehenderit eiusmod ut sunt irure culpa quis labore mollit amet. Ad minim reprehenderit ea reprehenderit proident minim.\r\n",
    "registered": "2016-07-25T11:58:02 +04:00",
    "latitude": 21.763497,
    "longitude": 72.512529,
    "tags": [
      "dolor",
      "velit",
      "sint",
      "ut",
      "aliqua",
      "ullamco",
      "anim"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Elena Kennedy"
      },
      {
        "id": 1,
        "name": "Evangelina Young"
      },
      {
        "id": 2,
        "name": "Sherman Maldonado"
      }
    ],
    "greeting": "Hello, Rhonda Booker! You have 10 unread messages.",
    "favoriteFruit": "apple"
  },
  {
    "_id": "6295906ded3cdb16e4be2ce4",
    "index": 4,
    "guid": "f8492363-7d84-4332-bd90-bba5ca01da49",
    "isActive": true,
    "balance": "$2,877.55",
    "picture": "http://placehold.it/32x32",
    "age": 40,
    "eyeColor": "brown",
    "name": "Clay Chan",
    "gender": "male",
    "company": "GINKLE",
    "email": "claychan@ginkle.com",
    "phone": "+1 (936) 554-3165",
    "address": "167 Meeker Avenue, Gratton, Maine, 304",
    "about": "Amet do et laborum qui culpa amet enim excepteur commodo exercitation. Reprehenderit id proident id aliqua proident commodo velit velit et culpa. Adipisicing enim mollit deserunt reprehenderit qui est et occaecat dolore adipisicing velit. Labore ipsum ex ex fugiat nulla ipsum dolor ad magna ullamco Lorem duis. Eu tempor ad enim consequat consequat. Tempor nulla sint laborum est cupidatat labore occaecat laboris. Mollit velit adipisicing enim do occaecat sunt.\r\n",
    "registered": "2018-07-27T07:19:36 +04:00",
    "latitude": -70.09504,
    "longitude": -99.196258,
    "tags": [
      "culpa",
      "sint",
      "sunt",
      "sint",
      "laboris",
      "commodo",
      "labore"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Brock Ashley"
      },
      {
        "id": 1,
        "name": "Lynn Acevedo"
      },
      {
        "id": 2,
        "name": "Margo Bernard"
      }
    ],
    "greeting": "Hello, Clay Chan! You have 2 unread messages.",
    "favoriteFruit": "banana"
  },
  {
    "_id": "6295906de5bfc560c69e8c53",
    "index": 5,
    "guid": "fc0d9a39-4cc5-4115-a864-bd9cf98235b9",
    "isActive": true,
    "balance": "$3,558.09",
    "picture": "http://placehold.it/32x32",
    "age": 31,
    "eyeColor": "green",
    "name": "Bridges Johnson",
    "gender": "male",
    "company": "PROFLEX",
    "email": "bridgesjohnson@proflex.com",
    "phone": "+1 (970) 552-3970",
    "address": "917 Hendrickson Place, Fontanelle, Mississippi, 3808",
    "about": "Anim adipisicing nulla laborum laboris culpa quis in officia occaecat non eu occaecat ex cillum. Pariatur labore amet commodo cupidatat sint dolore in aliqua deserunt qui. Culpa voluptate fugiat ut dolor. Incididunt excepteur qui aute proident tempor. Cupidatat ex eu fugiat est cillum commodo commodo.\r\n",
    "registered": "2020-06-20T07:24:16 +04:00",
    "latitude": 52.296492,
    "longitude": 81.015532,
    "tags": [
      "consectetur",
      "tempor",
      "laborum",
      "ad",
      "sunt",
      "ullamco",
      "nostrud"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Katy Ray"
      },
      {
        "id": 1,
        "name": "Cherry Buchanan"
      },
      {
        "id": 2,
        "name": "Katheryn Mcdonald"
      }
    ],
    "greeting": "Hello, Bridges Johnson! You have 10 unread messages.",
    "favoriteFruit": "strawberry"
  }
];

module.exports = USER_BD;
