const cloudflareScraper = require("cloudflare-scraper");
const express = require("express");

const app = express();
const PORT = 3000;

app.listen(PORT, (req, res) => {
  // indicamos que al momento de recibir una peticion por el puerto 3000 realice una accion
  console.log(`Server Encendido ${PORT}`);
});

app.get("/prueba", async (req, res) => {
  try {
    const response = await cloudflareScraper.get(
      "https://qpublic.schneidercorp.com/application.aspx?AppID=855&LayerID=15999&PageTypeID=4&PageID=7114&Q=464378692&KeyValue=00-2S-22-4520-000A-0210"
    );
    console.log(response);
    return res.send(response);
  } catch (error) {
    console.log(error);
    return res.send(error);
  }
});
